var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var appointmentSchema = new Schema({
  type: {
    type: String,
    required: true,
  },
  date: {
    type: String,
    required: true,
  },
  time: {
    type: String,
    required: true,
  }
});
module.exports = mongoose.model("appointment", appointmentSchema);
