"use strict";

const express = require("express");
const bodyParser = require("body-parser");
const app = express().use(bodyParser.json());

const config = require("./config");
const mongoose = require("mongoose");

var moment = require("moment");

const Appointment = require("./appointment");

var port = process.env.PORT || 8080;

var appointmentCount = 0;

async function getAppointments() {
  let appointments = [];
  try {
    appointments = await Appointment.find();
    console.log("appointments", appointments);
    appointments = appointments.map(
      (val, ind) =>
        `${ind + 1}. Your Appointment scheduled for ${val.type} on ${
          val.date
        } at ${val.time}`
    );
    console.log("Changes Appointments", appointments);
    appointmentCount = appointments.length;
    return appointments;
  } catch (err) {
    console.log(err);
    return "Error Occured";
  }
}

async function scheduleAppointment(parameters) {
  try {
    let appointment = new Appointment({
      type: parameters["appointment-type"],
      date: moment(parameters.date).format("MM-DD-YYYY"),
      time: parameters.time,
    });
    await appointment.save();
    return "Success";
  } catch (err) {
    console.log(err);
    return "Error Occured";
  }
}

app.post("/", async (req, res) => {
  console.log("intent action", req.body.queryResult);
  let action = req.body.queryResult.action;
  let parameters = req.body.queryResult.parameters;
  let intentName = req.body.queryResult.intent.displayName;
  console.log("action", action);
  console.log("parameters", parameters);
  console.log("intentName", intentName);
  if (action === "Get-Appointments") {
    return res.json({
      fulfillmentMessages: [
        {
          payload: {
            richContent: [
              [
                {
                  text: await getAppointments(),
                  title:
                    appointmentCount == 0
                      ? "You don't have any scheduled appointments"
                      : "Your Scheduled Appointments Are As Shown Below: ",
                  type: "description",
                },
              ],
              [
                {
                  options: [
                    {
                      text: "Restart Conversation",
                    },
                  ],
                  type: "chips",
                },
              ],
            ],
          },
        },
      ],
    });
  } else if (action === "Set-Appointment") {
    const result = await scheduleAppointment(parameters);
    return res.json({
      fulfillmentMessages: [
        {
          payload: {
            richContent: [
              [
                {
                  title:
                    result === "Success"
                      ? "Appointment has been scheduled successfully"
                      : "Some Error Occured, Please try again later",
                  type: "info",
                },
              ],
              [
                {
                  options: [
                    {
                      text: "Restart Conversation",
                    },
                  ],
                  type: "chips",
                },
              ],
            ],
          },
        },
      ],
    });
  }
});

mongoose.Promise = global.Promise;
mongoose.connect(config.dbUrl, () => {
  console.log("Mongo DB Connection Successfull");
});

app.listen(port, () =>
  console.log("Express server listening on port %d", port)
);
